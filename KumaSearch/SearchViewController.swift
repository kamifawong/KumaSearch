//
//  SearchViewController.swift
//  SearchBook
//
//  Created by Kuma Fai on 17/7/2017.
//  Copyright © 2017 Kuma Fai. All rights reserved.
//

import UIKit

protocol webDelegate {
    func search(barcode:String, url:String)
}

class SearchViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, cameraDelegate {
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet var searchBar: UISearchBar!
    
    var webDelegate : webDelegate? = nil
    var rowIndex = 0
    
    var pointA : (CGFloat, CGFloat) = (0.0, 0.0)
    var pointB = (UIScreen.main.bounds.width, UIScreen.main.bounds.height)

    
    let choice = ["Google", "Kingstone", "Amazon JP", "--Url--"]
    let url = ["https://www.google.com/#q=", "https://www.kingstone.com.tw/search/result.asp?c_name=", "https://www.amazon.co.jp/s/field-keywords=", ""]

    
    func numberOfComponents(in pickerView:
        UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return choice.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return choice[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        rowIndex = row
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.


        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchBar.resignFirstResponder()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    

//        if let touch = touches.first {
//        let pos : CGPoint = touch.location(in: self.view)
//        searchBar.text = "\(pos.x) , \(pos.y)"
//        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showCameraVC" {
            let cameraVC : CameraViewController = segue.destination as! CameraViewController
            cameraVC.cameraDelegate = self
        }
        
        if segue.identifier == "showWebVC" {
            let webVC : WebViewController = segue.destination as! WebViewController
            self.webDelegate = webVC
        }
   
    }
    
    func getBarcode(_ barcodeString: String) {
        searchBar.text = barcodeString
    }

    
    @IBAction func cameraButton(_ sender: UIButton) {
    }
    

    @IBAction func searchButton(_ sender: UIButton) {
        
        searchBar.resignFirstResponder()
        
        var urlSearch : String?

        if searchBar.text != String() {
            urlSearch = url[rowIndex] + searchBar.text!;
                if (choice[rowIndex] == "Kingstone") {
            urlSearch = urlSearch?.replacingOccurrences(of: " ", with: "%2520")
            } else {
                urlSearch = urlSearch?.replacingOccurrences(of: " ", with: "+")
            }
        } else {
            switch rowIndex {
            case 0:
                urlSearch = "https://www.google.com"
            case 1:
                urlSearch = "https://www.kingstone.com.tw"
            case 2:
                urlSearch = "https://www.amazon.co.jp"
            default:
                return
            }
        }
        
        webDelegate?.search(barcode:searchBar.text!, url:urlSearch!)

    }

}

