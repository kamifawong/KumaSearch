//
//  CameraViewController.swift
//  SearchBook
//
//  Created by Kuma Fai on 27/7/2017.
//  Copyright © 2017 Kuma Fai. All rights reserved.
//

import UIKit
import AVFoundation

protocol cameraDelegate {
    func getBarcode(_ barcodeString: String)
}

class CameraViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet var cancelButton: UIButton!
    
    var cameraDelegate: cameraDelegate? = nil
    
    var captureSession: AVCaptureSession?
    var previewLayer: AVCaptureVideoPreviewLayer?
    var codeFrameView: UIView?
    
    var metadataOutput: AVCaptureMetadataOutput?
    var captureDevice: AVCaptureDevice?
    var videoInput: AVCaptureDeviceInput?
    
    let codeTypes = [AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeITF14Code, AVMetadataObjectTypeInterleaved2of5Code, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode39Mod43Code,
                     AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
                     
                     AVMetadataObjectTypePDF417Code,
                     
                     AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode, AVMetadataObjectTypeDataMatrixCode]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        view.bringSubview(toFront: cancelButton)
        
        metadataOutput = AVCaptureMetadataOutput()
        captureSession = AVCaptureSession()
        captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        do {
            videoInput = try AVCaptureDeviceInput(device: captureDevice)
        } catch {
            print(error)
            return
        }
        
        captureSession?.addInput(videoInput)
        captureSession?.addOutput(metadataOutput)
        
        metadataOutput?.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        metadataOutput?.metadataObjectTypes = codeTypes
        
        previewLayer?.frame = view.layer.bounds
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        view.layer.addSublayer(previewLayer!)
        captureSession?.startRunning()
        self.view.bringSubview(toFront: cancelButton)

    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        for code in codeTypes {
            if metadataObj.type == code {
                
                let barcodeObject = previewLayer?.transformedMetadataObject(for: metadataObj)
                codeFrameView?.frame = barcodeObject!.bounds
                
                if cameraDelegate != nil {
                    if metadataObj.stringValue != nil {
                        let barcode = metadataObj.stringValue
                        cameraDelegate?.getBarcode(barcode!)
                        captureSession?.stopRunning()
                        dismiss(animated: true, completion: nil)

                    }
                }
            }
        }
        
    }
    

    @IBAction func cancelButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}
