//
//  WebViewController.swift
//  KumaSearch
//
//  Created by Kuma Fai on 28/7/2017.
//  Copyright © 2017 Kuma Fai. All rights reserved.
//

import UIKit
import os.log


var urlField: UITextField!
var itemBarcode: String?
var itemArray: [item] = []

let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height

open class item : NSObject, NSCoding {
    
    let name : String!
    let barcode : String!
    let url : String!
    let info : String!
//    let image : UIImage?
    
    static let dirPath = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let bookmarkURL = dirPath.appendingPathComponent("Bookmark")
    static let historyURL = dirPath.appendingPathComponent("History")
    
//    init(name: String!, barcode: String!, url: String!, info:String!, image: UIImage!) {
    init(name: String!, barcode: String!, url: String!, info: String!) {
        self.name = name
        self.barcode = barcode
        self.url = url
        self.info = info
//        self.image = image
    }
    required public init?(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.barcode = aDecoder.decodeObject(forKey: "barcode") as? String
        self.url = aDecoder.decodeObject(forKey: "url") as? String
        self.info = aDecoder.decodeObject(forKey: "info:") as? String
//        self.image = aDecoder.decodeObject(forKey: "image") as? UIImage
    }
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(barcode, forKey: "barcode")
        aCoder.encode(url, forKey: "url")
        aCoder.encode(info, forKey: "info")
//        aCoder.encode(image, forKey: "image")
    }

}

class WebViewController: UIViewController, UITextFieldDelegate, UIWebViewDelegate, webDelegate {
    
    var webView : UIWebView!
    var activityIndicator : UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let actionWidth = (screenWidth-15) / 7
        
        var actionButton = UIButton(frame: CGRect(x: 10, y: 20, width: actionWidth, height: actionWidth))
        actionButton.setTitle("Done", for: .normal)
        actionButton.setTitleColor(UIColor.blue, for: .normal)
        actionButton.addTarget(self, action: #selector(WebViewController.done), for: .touchUpInside)
        self.view.addSubview(actionButton)
        
        actionButton = UIButton(frame: CGRect(x: actionWidth*2, y: 20, width: actionWidth, height: actionWidth))
        actionButton.setImage(UIImage(named: "back") , for: .normal)
        actionButton.addTarget(self, action: #selector(WebViewController.back), for: .touchUpInside)
        self.view.addSubview(actionButton)
        
        actionButton = UIButton(frame: CGRect(x: actionWidth*3 + 10, y: 20, width: actionWidth, height: actionWidth))
        actionButton.setImage(UIImage(named: "forward") , for: .normal)
        actionButton.addTarget(self, action: #selector(WebViewController.forward), for: .touchUpInside)
        self.view.addSubview(actionButton)
        
        actionButton = UIButton(frame: CGRect(x: actionWidth*4 + 20, y: 20, width: actionWidth, height: actionWidth))
        actionButton.setImage(UIImage(named: "save") , for: .normal)
        actionButton.addTarget(self, action: #selector(WebViewController.save), for: .touchUpInside)
        self.view.addSubview(actionButton)
        
        actionButton = UIButton(frame: CGRect(x: actionWidth*6, y: 20, width: actionWidth, height: actionWidth))
        actionButton.setTitle("Go", for: .normal)
        actionButton.setTitleColor(UIColor.blue, for: .normal)
        actionButton.addTarget(self, action: #selector(WebViewController.go), for: .touchUpInside)
        self.view.addSubview(actionButton)
        
        urlField = UITextField(frame: CGRect(x: 5, y: 25 + actionWidth, width: screenWidth, height: 40))
        urlField.backgroundColor = UIColor.init(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        urlField.clearButtonMode = .whileEditing
        urlField.returnKeyType = .go
        urlField.delegate = self
        self.view.addSubview(urlField)
        
        webView = UIWebView(frame: CGRect(x: 0, y: 60 + actionWidth, width: screenWidth, height: screenHeight - 60 - actionWidth))
        webView.delegate = self
        self.view.addSubview(webView)
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicator.center = CGPoint(x: screenWidth / 2, y: screenHeight / 2)
        self.view.addSubview(activityIndicator)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    func done() {
        dismiss(animated: true, completion: nil)
    }
    func back() {
        self.view.endEditing(true)
        webView.goBack()

    }
    func forward() {
        self.view.endEditing(true)
        webView.goForward()
    }
    func save() {
        self.view.endEditing(true)
        
        let alertController = UIAlertController(
            title: "Save Bookmark",
            message: "",
            preferredStyle: .alert)
        
        alertController.addTextField { (name: UITextField) in
            name.placeholder = "Name"
        }
        alertController.addTextField { (barcode: UITextField) in
            barcode.text = itemBarcode
        }
        alertController.addTextField { (url: UITextField) in
            url.text = urlField.text
        }
        alertController.addTextField { (info: UITextField) in
            info.placeholder = "info"
        }
        
        let cancelAction = UIAlertAction(
            title: "Cancel",
            style: .cancel,
            handler: nil)
        
        let saveAction = UIAlertAction(
            title: "Save",
            style: UIAlertActionStyle.default) {
                (action: UIAlertAction!) -> Void in
                let newName : String! = alertController.textFields?.first?.text
                let newBarcode : String! = alertController.textFields?.dropFirst(1).first?.text
                let newUrl : String! = alertController.textFields?.dropFirst(2).first?.text
                let newInfo : String! = alertController.textFields?.dropFirst(3).first?.text
                let newItem = item(name: newName, barcode: newBarcode, url: newUrl, info: newInfo)
                

                let dataArray = NSKeyedUnarchiver.unarchiveObject(withFile: item.bookmarkURL.path) as! [item]
                    itemArray = dataArray
                
                itemArray.append(newItem)

                print(itemArray[0].name)
                print(itemArray[0].barcode)
                print(itemArray[0].url)
                print(itemArray[0].info)

                let saveSuccessful : Bool = NSKeyedArchiver.archiveRootObject(itemArray, toFile: item.bookmarkURL.path)
                
                if saveSuccessful {
                    
                    

                    let successAlert = UIAlertController(
                        title: "Bookmark saved",
                        message: "",
                        preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    successAlert.addAction(okAction)
                    self.present(successAlert, animated: true, completion: nil)
                } else {
                    let failureAlert = UIAlertController(
                        title: "Failed",
                        message: "",
                        preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    failureAlert.addAction(okAction)
                    self.present(failureAlert, animated: true, completion: nil)
                }

                
        }
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func go() {
        self.view.endEditing(true)
        
        let url = URL(string: urlField.text!)
        let urlRequest = URLRequest(url: url!)
        webView.loadRequest(urlRequest)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.go()
        
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator.startAnimating()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.stopAnimating()
        if let currentURL = webView.request?.url?.absoluteString {
            urlField.text = currentURL
        }
    }
    
    func search(barcode:String, url: String) {
        urlField.text = url
        itemBarcode = barcode
        self.go()
    }
    
}
