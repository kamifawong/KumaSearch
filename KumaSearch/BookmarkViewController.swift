//
//  BookmarkViewController.swift
//  KumaSearch
//
//  Created by Kuma Fai on 27/7/2017.
//  Copyright © 2017 Kuma Fai. All rights reserved.
//

import UIKit

class BookmarkViewController: UIViewController, UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    
    var searchController: UISearchController!
    
    var itemArray: [item] = []
    var filteredArray: [item] = []
    var searchActive = false
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        self.definesPresentationContext = true
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.delegate = self
        searchController.searchBar.delegate = self
        
        searchController.searchBar.returnKeyType = UIReturnKeyType.done
        searchController.searchBar.tintColor = UIColor.gray
        searchController.searchBar.barTintColor = UIColor.lightGray
        
        searchController.searchBar.showsCancelButton = false
        searchController.searchBar.showsSearchResultsButton = true
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        
        navigationItem.titleView = searchController.searchBar
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Item")
        tableView.delegate = self
        tableView.dataSource = self

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        print("Loaded.")

        loadItems()
        
        itemArray = itemArray.sorted(by: {$0.name < $1.name})
        tableView.reloadData()
    }
    

    func loadItems(){
        let dataArray = NSKeyedUnarchiver.unarchiveObject(withFile: item.bookmarkURL.path) as! [item]
        itemArray = dataArray
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            return filteredArray.count
        }
        return itemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Item", for: indexPath)
        cell.accessoryType = .detailButton
        
        if searchActive {
            cell.textLabel?.text = filteredArray.sorted(by: {$0.name < $1.name})[indexPath.row].name
        } else {
            cell.textLabel?.text = itemArray.sorted(by: {$0.name < $1.name})[indexPath.row].name
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            itemArray.remove(at: indexPath.row)
            NSKeyedArchiver.archiveRootObject(itemArray, toFile: item.bookmarkURL.path)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {

        // let alertController = UIAlertController(title: <#T##String?#>, message: <#T##String?#>, preferredStyle: <#T##UIAlertControllerStyle#>)
    }
    

    
    func updateSearchResults(for searchController: UISearchController) {
        filteredArray = itemArray.filter({(item) -> Bool in
            let loadItem:item! = item
            
            print(item.name)
            print(item.barcode)
            print(item.url)
            print(item.info)
            
            
            if item.name.range(of: searchController.searchBar.text!, options: NSString.CompareOptions.caseInsensitive) != nil {
              return true
            }
            if loadItem.barcode.range(of: searchController.searchBar.text!, options: NSString.CompareOptions.caseInsensitive) != nil {
                return true
            }
            if loadItem.info.range(of: searchController.searchBar.text!, options: NSString.CompareOptions.caseInsensitive) != nil {
                return true
            }
        

//            return (loadItem.name.range(of: searchController.searchBar.text!, options: NSString.CompareOptions.caseInsensitive) != nil
//                || loadItem.barcode.range(of: searchController.searchBar.text!, options: NSString.CompareOptions.caseInsensitive) != nil
//                || loadItem.info?.range(of: searchController.searchBar.text!, options: NSString.CompareOptions.caseInsensitive) != nil)
            
                    return false
        })

        
        if searchController.searchBar.text == nil || searchController.searchBar.text == "" {
            searchActive = false
        } else {
            searchActive = true
        }
        tableView.reloadData()
    }
}

